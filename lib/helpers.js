/*!
 * helpers.js - CLI Helper Functions
 *
 * Copyright (c) 2015 Cisco Systems, Inc. See LICENSE file.
 */

"use strict";

var jose = require("node-jose"),
    fs = require("fs"),
    path = require("path"),
    stream = require("stream");

function catchCommand(fn) {
  return function() {
    return fn.apply(this, arguments).
           catch((err) => {
            var code = -1;
            if (err) {
              console.error("FAILED: " + err.message);
              code = err.errno || code;
            }
            process.exit(code);
           });
  };
}

function wrapWritableStream(ws) {
  var wws = new stream.Writable({
    write: function(chunk, enc, next) {
      if (Buffer.isBuffer(chunk)) {
        ws.write(chunk);
      } else {
        ws.write(chunk, enc);
      }
      next();
    }
  });
  ws.on("error", (err) => {
    wws.emit("error", err);
  });
  return wws;
}

function promisifyReadable(rs) {
  return new Promise((resolve, reject) => {
    var data = [],
        len = 0;
    rs.on("readable", () => {
      var chunk = rs.read();
      if (chunk) {
        len += chunk.length;
        data.push(chunk);
      }
    });
    rs.on("end", () => {
      data = Buffer.concat(data, len);
      resolve(data);
    });
    rs.on("error", (err) => {
      reject(err);
    });
  });
}
function promisifyWritable(ws) {
  return new Promise((resolve, reject) => {
    ws.on("finish", () => {
      resolve();
    });
    ws.on("error", (err) => {
      reject(err);
    });
  });
}

function setupInputFrom(input, enc) {
  if (input) {
    input = path.normalize(input);
    input = fs.createReadStream(input, {
      flags: "r",
      autoClose: true
    });
  } else {
    input = process.stdin;
  }
  if (enc) {
    input.setEncoding(enc);
  }

  return input;
}
function setupOutputTo(output, enc) {
  if (output) {
    // TODO: create missing directories to {out}
    output = path.normalize(output);
    output = fs.createWriteStream(output, {
      flags: "w",
      mode: 0x180   // user:r, group: , other:
    });
  } else {
    output = process.stdout;
  }
  if (enc) {
    output.setEncoding(enc);
  }

  output = wrapWritableStream(output);
  return output;
}

function readKeyStore(store) {
  store = path.normalize(store);
  return new Promise((resolve, reject) => {
    fs.access(store, fs.R_OK | fs.W_OK, (err) => {
      var ks;
      if (err) {
        if ("ENOENT" !== err.code) {
          return reject(err);
        }
        ks = jose.JWK.createKeyStore();
        ks.path = store;
        resolve(ks);
      }
      fs.readFile(store, "utf8", (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(jose.JWK.asKeyStore(data).
                     then((ks) => {
                       ks.path = store;
                       return ks;
                     }));
      });
    });
  });
}
function writeKeyStore(ks) {
  if (!ks.path) {
    return Promise.resolve();
  }
  var out;
  out = fs.createWriteStream(ks.path, {
    flags: "w",
    mode: 0x180   // user:r, group: , other:
  });
  out = wrapWritableStream(out);

  var data = JSON.stringify(ks.toJSON(true));
  var p = promisifyWritable(out);
  out.write(data);
  out.write("\n");
  out.end();

  return p;
}

function readKey(input, format, ks) {
  if (!ks) {
    ks = jose.JWK.createKeyStore();
  }

  input = setupInputFrom(input);
  var promise = promisifyReadable(input);
  // import key
  promise = promise.then((input) => {
    if (!input.length) {
      return Promise.reject(new Error("no data"));
    }
    return ks.add(input, format);
  });
  return promise;
}

module.exports = {
  command: catchCommand,
  keyStore: {
    read: readKeyStore,
    write: writeKeyStore
  },
  key: {
    read: readKey
  },
  wrap: wrapWritableStream,
  input: setupInputFrom,
  output: setupOutputTo,
  promisify: {
    input: promisifyReadable,
    output: promisifyWritable
  }
};
