/*!
 * actions/decode.js - "Decode data" action
 *
 * Copyright (c) 2015 Cisco Systems, Inc. See LICENSE file.
 */

"use strict";

var jose = require("node-jose"),
    helpers = require("../helpers");

module.exports = {
  command: "decode",
  describe: "Encodes/decodes data to/from various formats",
  builder: (yargs) => {
    yargs = yargs.
        option("in", {
          desc: "file to read data from (defaults to stdin)",
          requiresArg: true
        }).
        option("inform", {
          choices: ["binary", "base64", "base64url", "hex", "utf8"],
          default: "base64url",
          desc: "format of input",
          requiresArg: true
        }).
        option("out", {
          desc: "file to write data to (defaults to stdout)",
          requiresArg: true
        }).
        option("outform", {
          choices: ["binary", "base64", "base64url", "hex", "utf8"],
          default: "binary",
          desc: "format of output",
          requiresArg: true
        });
    return yargs;
  },
  handler: helpers.command((argv) => {
    var input = helpers.input(argv.in),
        output = helpers.output(argv.out);

    // TODO: be more efficient
    var promise = helpers.promisify.input(input);
    promise = promise.then((data) => {
      // strip whitespace
      data = data.toString("binary");
      data = data.trim();
      data = Buffer.from(data, "binary");

      // convert input
      switch (argv.inform) {
        case "binary":
          // DO NOTHING
          break;
        case "base64url":
          data = jose.util.base64url.decode(data);
          break;
        default:
          data = data.toString("binary");
          data = Buffer.from(data, argv.inform);
          break;
      }

      // convert output
      switch (argv.outform) {
        case "binary":
          // DO NOTHING
          break;
        case "base64url":
          data = jose.util.base64url.encode(data);
          data = Buffer.from(data, "ascii");
          break;
        default:
          data = data.toString(argv.outform);
          data = Buffer.from(data, "binary");
          break;
      }

      output.write(data);
      output.write("\n", "utf8")
      output.end();
      return helpers.promisify.output(output);
    });

    return promise;
  })
};
