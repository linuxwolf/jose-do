/*!
 * actions/sign.js - "Sign content" action
 *
 * Copyright (c) 2015 Cisco Systems, Inc. See LICENSE file.
 */

"use strict";

var jose = require("node-jose"),
    helpers = require("../helpers");

module.exports = {
  command: "sign",
  describe: "sign content",
  builder: (yargs) => {
    yargs = yargs.
        option("key", {
          desc: "file to read key from",
          requiresArg: true,
          required: true
        }).
        option("keyform", {
          choices: ["json", "private", "public", "pkcs8", "spki", "x509", "pkix", "pem"],
          default: "json",
          desc: "format of key",
          requiresArg: true
        }).
        option("alg", {
          desc: "algorithm for signing content",
          requiresArg: true
        }).
        option("cty", {
          desc: "media-type of content",
          requiresArg: true
        }).
        option("in", {
          desc: "file to read plaintext content from (defaults to stdin)",
          requiresArg: true
        }).
        option("out", {
          desc: "file to write encrypted content to (defaults to stdout)",
          requiresArg: true
        }).
        option("outform", {
          choices: ["compact", "general", "flattened"],
          default: "compact",
          desc: "format of output",
          requiresArg: true
        });
    return yargs;
  },
  handler: helpers.command((argv) => {
    // import key
    var promise;
    promise = Promise.all([
      // import key
      helpers.key.read(argv.key, argv.keyform),
      // read content
      function() {
        return helpers.promisify.input(helpers.input(argv.in));
      }()
    ]);
    // encrypt!
    promise = promise.then(function(results) {
      var key = results[0],
          input = results[1];

      var options = {
        format: argv.outform,
        fields: {}
      };
      if (argv.cty) {
        options.fields.cty = argv.cty;
      }

      var signer = {
        key: key,
        header: {}
      };
      if (argv.alg) {
        signer.header.alg = argv.alg;
      }
      return jose.JWS.createSign(options, signer).
             update(input).
             final();
    });
    // write output
    promise = promise.then(function(jws) {
      var out = helpers.output(argv.out);
      if ("string" !== typeof jws) {
        jws = JSON.stringify(jws);
      }
      var p = helpers.promisify.output(out);
      out.write(jws);
      out.write("\n");
      out.end();
      return p;
    });

    return promise;
  })
};
