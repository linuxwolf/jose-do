/*!
 * commands/10-generate.js - "Generate Key" action
 *
 * Copyright (c) 2015 Cisco Systems, Inc. See LICENSE file.
 */

"use strict";

var jose = require("node-jose"),
    helpers = require("../helpers");

module.exports = {
  command: "generate",
  describe: "Generate a new key",
  builder: (yargs) => {
    yargs = yargs.
        option("kty", {
          choices: ["EC", "RSA", "oct"],
          desc: "type of key to generate",
          demand: true
        }).
        option("size", {
          desc: "size of the generated key, or named curve to use",
          demand: true
        }).
        option("kid", {
          desc: "identifier for the generated key",
          requiresArg: true
        }).
        option("alg", {
          desc: "algorithm the key is valid for",
          requiresArg: true
        }).
        option("use", {
          choices: ["enc", "sig"],
          desc: "use the key is valid for",
          requiresArg: true
        }).
        option("out", {
          desc: "file to write key to (defaults to stdout)",
          requiresArg: true
        }).
        option("store", {
          desc: "keystore to include key in",
          requiresArg: true
        });
    return yargs;
  },
  handler: helpers.command((argv) => {
    // validate inputs
    var opts = {};
    if (argv.alg) {
      opts.alg = argv.alg;
    }
    if (argv.use) {
      opts.use = argv.use;
    }
    if (argv.kid) {
      opts.kid = argv.kid;
    }

    var promise;
    // setup keystore
    if (argv.store) {
      promise = helpers.keyStore.read(argv.store);
    } else {
      promise = Promise.resolve(jose.JWK.createKeyStore());
    }
    // generate key
    promise = promise.then(function(ks) {
      return ks.generate(argv.kty, argv.size, opts);
    });
    // output key
    promise = promise.then(function(key) {
      var out = helpers.output(argv.out);

      var data = JSON.stringify(key.toJSON(true));
      var p = helpers.promisify.output(out);
      out.write(data);
      out.write("\n");
      out.end();

      return p.then(function() {
        return key.keystore;
      });
    });
    promise = promise.then(function(ks) {
      return helpers.keyStore.write(ks);
    });
    return promise;
  })
};
