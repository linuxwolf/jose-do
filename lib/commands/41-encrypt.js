/*!
 * actions/encrypt.js - "Encrypt content" action
 *
 * Copyright (c) 2015 Cisco Systems, Inc. See LICENSE file.
 */

"use strict";

var crypto = require("crypto"),
    jose = require("node-jose"),
    helpers = require("../helpers");

module.exports = {
  command: "encrypt",
  describe: "encrypt content",
  builder: (yargs) => {
    yargs = yargs.
        option("key", {
          desc: "file to read key from",
          requiresArg: true,
          required: true
        }).
        option("keyform", {
          choices: ["json", "private", "public", "pkcs8", "spki", "x509", "pkix", "pem"],
          default: "json",
          desc: "format of input (defaults to json)",
          requiresArg: true
        }).
        option("alg", {
          desc: "algorithm for encrypting content key",
          requiresArg: true
        }).
        option("enc", {
          desc: "algorithm for encrypting content",
          requiresArg: true
        }).
        option("p2s", {
          desc: "PBES2 salt (if using a password-based algorithm)",
          requiresArg: true
        }).
        option("p2c", {
          desc: "PBES2 iteration count (if using a password-based algorithm)",
          number: true,
          default: 8192,
          requiresArg: true
        }).
        option("cty", {
          desc: "media-type of content",
          requiresArg: true
        }).
        option("zip", {
          desc: "compress content before encrypting?"
        }).
        option("in", {
          desc: "file to read plaintext content from (defaults to stdin)",
          requiresArg: true
        }).
        option("out", {
          desc: "file to write encrypted content to (defaults to stdout)",
          requiresArg: true
        }).
        option("outform", {
          choices: ["compact", "general", "flattened"],
          default: "compact",
          desc: "format of output (defauls to compact)",
          requiresArg: true
        });
    return yargs;
  },
  handler: helpers.command((argv) => {
    // import key
    var promise;
    promise = Promise.all([
      // import key
      helpers.key.read(argv.key, argv.keyform),
      // read content
      function() {
        return helpers.promisify.input(helpers.input(argv.in));
      }()
    ]);
    // encrypt!
    promise = promise.then(function(results) {
      var key = results[0],
          input = results[1];

      var fields = {};
      if (argv.cty) {
        fields.cty = argv.cty;;
      }

      var options = {
        format: argv.outform,
        zip: argv.zip,
        fields: fields
      };
      if (argv.enc) {
        options.contentAlg = argv.enc;
      }

      var recipient = {
        key: key,
        header: {}
      };
      if (argv.alg) {
        recipient.header.alg = argv.alg;
        if (argv.alg.startsWith("PBES2-")) {
          recipient.header.p2c = argv.p2c;
          recipient.header.p2s = argv.p2s || jose.util.base64url.encode(crypto.randomBytes(16));
        }
      }
      return jose.JWE.createEncrypt(options, recipient).
             update(input).
             final();
    });
    // write output
    promise = promise.then(function(jwe) {
      var out = helpers.output(argv.out);
      if ("string" !== typeof jwe) {
        jwe = JSON.stringify(jwe);
      }
      var p = helpers.promisify.output(out);
      out.write(jwe);
      out.write("\n");
      out.end();
      return p;
    });

    return promise;
  })
};
