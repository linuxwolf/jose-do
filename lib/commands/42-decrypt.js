/*!
 * actions/decrypt.js - "Decrypt content" action
 *
 * Copyright (c) 2015 Cisco Systems, Inc. See LICENSE file.
 */

"use strict";

var jose = require("node-jose"),
    helpers = require("../helpers");

module.exports = {
  command: "decrypt",
  describe: "decrypt content",
  builder: (yargs) => {
    yargs = yargs.
        option("store", {
          desc: "file to read keystore from",
          requiresArg: true
        }).
        option("key", {
          desc: "file to read key from",
          requiresArg: true
        }).
        option("keyform", {
          choices: ["json", "private", "public", "pkcs8", "spki", "x509", "pkix", "pem"],
          default: "json",
          desc: "format of key (defaults to json)",
          requiresArg: true
        }).
        option("in", {
          desc: "file to read encrypted content from (defaults to stdin)",
          requiresArg: true
        }).
        option("inform", {
          choices: ["compact", "general", "flattened"],
          default: "compact",
          desc: "format of input (defaults to compact)",
          requiresArg: true
        }).
        option("out", {
          desc: "file to write plaintext content to (defaults to stdout)",
          requiresArg: true
        });
    return yargs;
  },
  handler: helpers.command((argv) => {
    if (!argv.key && !argv.store) {
      return Promise.reject(new Error("Require either key or store"));
    }

    var promise = Promise.resolve();
    if (argv.store) {
      promise = promise.then(function() {
        return helpers.keyStore.read(argv.store);
      });
    }
    if (argv.key) {
      promise = promise.then(function(store) {
        if (!store) {
          store = jose.JWK.createKeyStore();
        }
        return helpers.key.read(argv.key, argv.keyform, store);
      });
    }
    promise = promise.then(function(key) {
      var p = helpers.promisify.input(helpers.input(argv.in));
      return p.then(function(input) {
        // coerce input into desired format
        input = input.toString("utf8");
        switch (argv.inform) {
          case "general":
            /* eslint no-fallthrough: [0] */
          case "flattened":
            input = JSON.parse(input);
            break;
        }
        return [input, key];
      });
    });
    promise = promise.then(function(results) {
      var input = results[0],
          keystore = results[1];
      return jose.JWE.createDecrypt(keystore).
             decrypt(input);
    });
    // output plaintext
    promise = promise.then(function(results) {
      var out = helpers.output(argv.out);
      var p = helpers.promisify.output(out);
      out.write(results.plaintext);
      out.end();
      return p;
    });
    return promise;
  })
};
