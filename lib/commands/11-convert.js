/*!
 * actions/convert.js - "Convert Key Formats" action
 *
 * Copyright (c) 2015 Cisco Systems, Inc. See LICENSE file.
 */

"use strict";

var jose = require("node-jose"),
    helpers = require("../helpers");

module.exports = {
  command: "convert",
  describe: "Convert JWK / PKCS8 / SPKI / PKIX key formats to JWK",
  builder: (yargs) => {
    yargs = yargs.
        option("in", {
          desc: "file to read key from (defaults to stdin)",
          requiresArg: true
        }).
        option("inform", {
          choices: ["json", "private", "public", "pkcs8", "spki", "x509", "pkix", "pem"],
          default: "pem",
          desc: "format of input (defaults to pem)",
          requiresArg: true
        }).
        option("kid", {
          desc: "identifier for the input key",
          requiresArg: true
        }).
        option("out", {
          desc: "file to write converted key to (defaults to stdout)",
          requiresArg: true
        }).
        option("public", {
          desc: "output public key only"
        }).
        option("store", {
          desc: "keystore to include key in",
          requiresArg: true
        });
    return yargs;
  },
  handler: helpers.command((argv) => {
    var promise;
    // setup keystore
    promise = Promise.all([
      (argv.store) ? helpers.keyStore.read(argv.store) :
                     Promise.resolve(jose.JWK.createKeyStore()),
      helpers.promisify.input(helpers.input(argv.in))
    ]);
    // convert/import key
    promise = promise.then(function(results) {
      var keystore = results[0],
          input = results[1];

      if (!input.length) {
        return Promise.reject(new Error("no data"));
      }
      var extra = {};
      if (argv.kid) {
        extra.kid = argv.kid;
      }
      if (argv.alg) {
        extra.alg = argv.alg;
      }
      if (argv.use) {
        extra.use = argv.use;
      }
      return keystore.add(input, argv.inform, extra);
    });
    // write key
    promise = promise.then(function(key) {
      var out = helpers.output(argv.out);

      var data = JSON.stringify(key.toJSON(!argv.public));
      var p = helpers.promisify.output(out);
      out.write(data);
      out.write("\n");
      out.end();

      return p.then(function() {
        return key.keystore;
      });
    });
    // (OPTIONAL) write keystore
    promise = promise.then(function(ks) {
      return helpers.keyStore.write(ks);
    });
    return promise;
  })
};
