/*!
 * cmd.js - Command-line Entry Point
 *
 * Copyright (c) 2015 Cisco Systems, Inc. See LICENSE file.
 */

"use strict";

var fs = require("fs"),
    path = require("path"),
    helpers = require("../helpers");

module.exports = {
  command: "version",
  describe: "Print current version",
  handler: helpers.command((/** argv */) => {
    var pkg = fs.readFileSync(path.resolve(__dirname, "../../package.json"));
    pkg = JSON.parse(pkg);
    console.log(pkg.version);

    return Promise.resolve();
  })
};
