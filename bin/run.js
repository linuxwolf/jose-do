#!/usr/bin/env node
/*!
 * run.js - Command-line Entry Point
 *
 * Copyright (c) 2015 Cisco Systems, Inc. See LICENSE file.
 */

/* eslint no-process-exit: [0] */
"use strict";

var yargs = require("yargs"),
    path = require("path");

var argv = yargs.
           commandDir(path.join(__dirname, "..", "lib", "commands")).
           demand(1).
           completion("bash").
           help().
           argv;
