# JOSE-DO #

JOSE DO SOME CRYPTO!

A command-line tool to perform various JOSE cryptographic operations.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Prerequisites](#markdown-header-prerequisites)
- [Installing](#markdown-header-installing)
- [Using](#markdown-header-using)
    - [Generate](#markdown-header-generate)
    - [Convert](#markdown-header-convert)
    - [Sign](#markdown-header-sign)
    - [Verify](#markdown-header-verify)
    - [Encrypt](#markdown-header-encrypt)
    - [Decrypt](#markdown-header-decrypt)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Prerequisites ##

This tool requires the following:

* [node](https://nodejs.org/) - Version 0.10 or higher


## Installing ##

To install this tool:

```
npm install --global git+https://bitbucket-eng-sjc1.cisco.com/bitbucket/scm/cisrr/jose-do.git#latest
```


## Using ##


### Generate ###

Generates a new key.  The generated key is formatted as a JSON Web Key (JWK).

Options:

* `--kty` (**REQUIRED**) - The type of key to generate. One of `EC`, `RSA`, `oct`
* `--size` (**REQUIRED**) - The size of the key. For `RSA` and `oct` this is the key length in bits (e.g., `2048`); for `EC` this is the named curve (e.g., `P-256`)
* `--kid` - The identifier for the generated key
* `--alg` - The algorithm the key is valid for (e.g., `RS256` or `A128GCM`)
* `--use` - The usage this key is valid for. Either `sig` or `enc`
* `--out` - The file to write the key to (defaults to `stdout`)
* `--store` - The JWK-keystore to include the key in


### Convert ###

Converts a key to a JWK.  This command is used for both:

* converting another key format into a JWK
* extracting from a private JWK to a public JWK

Options:

* `--in` - The file to read the key from (defaults to `stdin`)
* `--inform` - The format of the input key. One of `json`, `public`, `private`, `pkcs8`, `spki`, `pkix`, `pem` (defaults to `pem`).
* `--kid` - The identifier for the input key
* `--out` - The file to write the key to (defaults to `stdout`)
* `--public` - Output the public key
* `--store` - The JWK-keystore to include the key in


### Sign ###

Signs content using a given key.

Options:

* `--key` (**REQUIRED**) - The file to read the signing key from.
* `--keyform` - The format of the signing key. One of `json`, `public`, `private`, `pkcs8`, `spki`, `pkix`, `pem` (defaults to `json`).
* `--alg` - The algorithm to use when signing the content. Defaults to the best algorithm for the signing key.
* `--cty` - The media-type for the signing content.
* `--in` - The file to read plaintext content from (defaults to `stdin`).
* `--out` - The file to write the JWS object to (defaults to `stdout`).
* `--outform` - The format of the output JWS object. One of `compact`, `general`, or `flattened` (defaults to `compact`).


### Verify ###

Verifies a JSON Web Signature (JWS) object.

Options:

**NOTE:** either `--key` or `--store` is **REQUIRED**.

* `--store` - The file to read the JWK-keystore from.
* `--key` - The file to read the verifying key from.
* `--keyform` - The format of the signing key. One of `json`, `public`, `private`, `pkcs8`, `spki`, `pkix`, `pem` (defaults to `json`).
* `--in` - The file to read the JWS object from (defaults to `stdin`).
* `--outform` - The format of the input JWS object. One of `compact`, `general`, or `flattened` (defaults to `compact`).
* `--out` - The file to write the plaintext content to (defaults to `stdout`).


### Encrypt ###

Encrypts content using a given key.

Options:

* `--key` (**REQUIRED**) - The file to read the encrypting key from.
* `--keyform` - The format of the encrypting key. One of `json`, `public`, `private`, `pkcs8`, `spki`, `pkix`, `pem` (defaults to `json`).
* `--alg` - The algorithm to use when encrypting the content. Defaults to the best algorithm for the encryption key.
* `--cty` - The media-type for the encrypting content.
* `--in` - The file to read plaintext content from (defaults to `stdin`).
* `--out` - The file to write the JWE object to (defaults to `stdout`).
* `--outform` - The format of the output JWE object. One of `compact`, `general`, or `flattened` (defaults to `compact`).


### Decrypt ###

Decrypts a JSON Web Encryption (JWE) object.

Options:

**NOTE:** either `--key` or `--store` is **REQUIRED**.

* `--store` - The file to read the JWK-keystore from.
* `--key` - The file to read the decrypting key from.
* `--keyform` - The format of the decrypting key. One of `json`, `public`, `private`, `pkcs8`, `spki`, `pkix`, `pem` (defaults to `json`).
* `--in` - The file to read the JWE object from (defaults to `stdin`).
* `--outform` - The format of the input JWE object. One of `compact`, `general`, or `flattened` (defaults to `compact`).
* `--out` - The file to write the plaintext content to (defaults to `stdout`).
